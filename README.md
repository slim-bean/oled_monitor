

Some of the log4rs (and maybe other deps) build some native code and need to have the cross compiler on the path:

```shell
export PATH=$PATH:/home/user/cc/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf/bin/
```