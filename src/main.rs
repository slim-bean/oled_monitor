#[macro_use]
extern crate log;
extern crate log4rs;
extern crate chrono;
extern crate mosquitto_client;
extern crate serde_json;
extern crate serde_yaml;

extern crate sensor_lib;
extern crate ssd1306;
extern crate embedded_graphics;

extern crate embedded_hal as hal;
extern crate linux_embedded_hal as linux_hal;

use sensor_lib::{TempHumidityValue,WindSpeedDirValue};

use embedded_graphics::fonts::{Font12x16,Font6x12};
use embedded_graphics::prelude::*;

use ssd1306::prelude::*;
use ssd1306::Builder;

use linux_hal::{I2cdev};

use std::{io, fs};
use std::io::prelude::*;

use std::slice::Split;

use std::rc::Rc;
use std::cell::RefCell;

use std::path::Path;

fn main() {
    //Check if there is a logging config configured in /etc if not just use one local to the app
    let etc_config = Path::new("/etc/oled_monitor/log4rs.yml");
    let log_config_path = if let true = etc_config.exists() {
        etc_config
    } else {
        Path::new("log4rs.yml")
    };
    log4rs::init_file(log_config_path, Default::default()).expect("Failed to init logger");

    info!("Starting oled_monitor");

    info!("Opening I2C device");
    let dev = I2cdev::new("/dev/i2c-1").unwrap();

    let mut disp: GraphicsMode<_> = Builder::new().connect_i2c(dev).into();

    disp.init().unwrap();
    disp.flush().unwrap();

    let disp_callback = Rc::new(RefCell::new(disp));
    let disp_main = disp_callback.clone();

    info!("Connecting to MQ");

    let m = mosquitto_client::Mosquitto::new("oled_monitor");
    m.connect_wait("mq.edjusted.com", 1883, 5000, Some(60)).expect("Failed to connect");
    let outside_temp_humidity = m.subscribe("/ws/1/grp/temp_humidity", 2).expect("can't subscribe to /ws");
    let inside_temp_humidity = m.subscribe("/ws/2/grp/temp_humidity", 2).expect("can't subscribe to /ws");
    let wind_speed_dir = m.subscribe("/ws/4/grp/wind_speed_dir", 2).expect("can't subscribe to /ws");

    info!("Connected and subscribed");




    let mut mc = m.callbacks(disp_callback);
    mc.on_message( |disp_callback,msg| {
        if outside_temp_humidity.matches(&msg) {
            match serde_json::from_slice::<TempHumidityValue>(msg.payload()){
                Ok(val) => {
                    info!("Received {:?}", val);

                    info!("Updating Outside Temp");
                    let int_val = val.temp.round() as i32;
                    let formatted = format!("O:{:3}F", int_val);
                    disp_callback.borrow_mut().draw(
                        Font12x16::render_str(formatted.as_str())
                            .with_stroke(Some(1u8.into()))
                            .into_iter(),
                    );
                    disp_callback.borrow_mut().flush().unwrap();

                    info!("Updating Outside Humidity");
                    let mut int_val = val.humidity.round() as i32;
                    if int_val > 99{
                        int_val = 99;
                    }
                    let formatted = format!("{:2}%", int_val);
                    disp_callback.borrow_mut().draw(
                        Font12x16::render_str(formatted.as_str())
                            .with_stroke(Some(1u8.into()))
                            .translate(Coord::new(90, 0))
                            .into_iter(),
                    );
                    disp_callback.borrow_mut().flush().unwrap();

                },
                Err(err) => {
                    error!("Failed to deserialize message: {}", err);
                },
            };
        } else if inside_temp_humidity.matches(&msg) {
            match serde_json::from_slice::<TempHumidityValue>(msg.payload()){
                Ok(val) => {
                    info!("Updating Inside Temp");
                    let int_val = val.temp.round() as i32;
                    let formatted = format!("I:{:3}F", int_val);
                    disp_callback.borrow_mut().draw(
                        Font12x16::render_str(formatted.as_str())
                            .with_stroke(Some(1u8.into()))
                            .translate(Coord::new(0, 18))
                            .into_iter(),
                    );
                    disp_callback.borrow_mut().flush().unwrap();

                    info!("Updating Inside Humidity");
                    let mut int_val = val.humidity.round() as i32;
                    if int_val > 99{
                        int_val = 99;
                    }
                    let formatted = format!("{:2}%", int_val);
                    disp_callback.borrow_mut().draw(
                        Font12x16::render_str(formatted.as_str())
                            .with_stroke(Some(1u8.into()))
                            .translate(Coord::new(90, 18))
                            .into_iter(),
                    );
                    disp_callback.borrow_mut().flush().unwrap();
                },
                Err(err) => {
                    error!("Failed to deserialize message: {}", err);
                },
            }


        } else if wind_speed_dir.matches(&msg) {
            match serde_json::from_slice::<WindSpeedDirValue>(msg.payload()){
                Ok(val) => {
                    let int_val = val.speed;
                    let formatted = format!("W: {:2}", int_val);
                    disp_callback.borrow_mut().draw(
                        Font12x16::render_str(formatted.as_str())
                            .with_stroke(Some(1u8.into()))
                            .translate(Coord::new(0, 36))
                            .into_iter(),
                    );
                    disp_callback.borrow_mut().flush().unwrap();

                    let mut int_val = val.dir;
                    let mut dir = "N";
                    if 338 < int_val && int_val <= 23 {
                        dir = "N";
                    } else if 23 < int_val && int_val <= 68 {
                        dir = "NE";
                    } else if 68 < int_val && int_val <= 113 {
                        dir = "E";
                    } else if 113 < int_val && int_val <= 158 {
                        dir = "SE";
                    } else if 158 < int_val && int_val <= 203 {
                        dir = "S";
                    } else if 203 < int_val && int_val <= 248 {
                        dir = "SW";
                    } else if 248 < int_val && int_val < 293 {
                        dir = "W";
                    } else {
                        dir = "NW";
                    }

                    let formatted = format!("{:2}", dir);
                    disp_callback.borrow_mut().draw(
                        Font12x16::render_str(formatted.as_str())
                            .with_stroke(Some(1u8.into()))
                            .translate(Coord::new(90, 36))
                            .into_iter(),
                    );
                    disp_callback.borrow_mut().flush().unwrap();
                },
                Err(err) => {
                    error!("Failed to deserialize message: {}", err);
                },
            }
        } else {
            info!("UnMatched Message: {:?}", msg.payload());
        }

    });

    mc.on_subscribe(|_, id|{
        info!("Subscribed: {}", id);
    });

    let mut counter = 0;
    loop {
        m.do_loop(500).unwrap();
        counter = counter + 1;
        if counter >= 2 {

            let mut temp = 0.0;
            match read_file("/sys/class/thermal/thermal_zone0/temp") {
                Ok(data) => {
                    match data.trim().parse::<f32>() {
                        Ok(x) => {
                            temp = x;
                        }
                        Err(e) => println!("Error: {}", e),
                    };
                },
                Err(_) => {},
            }

            let mut one_min = 0.0;
            let mut five_min = 0.0;
            let mut fifteen_min = 0.0;
            match read_file("/proc/loadavg") {
                Ok(data) => {

                    let mut entries = data.split(' ');
                    //I manually added some type defs here because CLion wasn't able to deduce them
                    let option:Option<f32> = entries.next()
                        .and_then(|entry:&str|{
                            match entry.parse::<f32>() {
                                Ok(flt) => {Some(flt)},
                                Err(_) => {None},
                            }
                        });
                    match option {
                        Some(flt) => {
                            one_min = flt;
                        },
                        None => {warn!("Failed to parse load average to float");},
                    }
                    let option:Option<f32> = entries.next()
                        .and_then(|entry:&str|{
                            match entry.parse::<f32>() {
                                Ok(flt) => {Some(flt)},
                                Err(_) => {None},
                            }
                        });
                    match option {
                        Some(flt) => {
                            five_min = flt;
                        },
                        None => {warn!("Failed to parse load average to float");},
                    }
                    let option:Option<f32> = entries.next()
                        .and_then(|entry:&str|{
                            match entry.parse::<f32>() {
                                Ok(flt) => {Some(flt)},
                                Err(_) => {None},
                            }
                        });
                    match option {
                        Some(flt) => {
                            fifteen_min = flt;
                        },
                        None => {warn!("Failed to parse load average to float");},
                    }
                },
                Err(_) => {},
            }

            let formatted = format!("{:2}C {:.2}/{:.2}/{:.2}", (temp/1000.0).round(), one_min, five_min, fifteen_min);
            disp_main.borrow_mut().draw(
                Font6x12::render_str(formatted.as_str())
                    .with_stroke(Some(1u8.into()))
                    .translate(Coord::new(0, 52))
                    .into_iter(),
            );
            disp_main.borrow_mut().flush().unwrap();

            counter = 0;
        }

    }

}


fn read_file(path: &str) -> io::Result<String> {
    let mut s = String::new();
    fs::File::open(path)
        .and_then(|mut f| f.read_to_string(&mut s))
        .map(|_| s)
}
